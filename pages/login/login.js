// pages/login.js
Page({
  formSubmit(e){
    let { username, code } = e.detail.value
    // 把用户信息存入storage
    wx.setStorage({
        'key': username,
        'data': '',
    })
    // 有用户名并且验证码正确，跳转到服务中心页面
    if(username && code == 666){
      console.log('6666')
      wx.redirectTo({
        url: '../../pages/service/service',
      })
    }
   
  },
  getCode(){
    console.log('获取验证码')
  },
  verify(e){
    let code = e.detail.value
   if(code != 666){
    this.setData({
      isActive:'visible'
    })
   }else{
     this.setData({
       isActive: 'hidden'
     })
   }
  },
  /**
   * 页面的初始数据
   */
  data: {
    isActive:'hidden'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})